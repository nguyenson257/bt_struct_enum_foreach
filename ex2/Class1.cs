﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex2
{
    internal class Class1
    {
        struct SinhVien
        {
            public int MaSo_146;
            public string HoTen_146;
            public Lopsh Lopsh_146;
            public double DiemTB_146;
        }
        enum Lopsh
        {
            T1=1,
            T2=2,
            T3=3,
            T4=4
        }

        static void XuatThongTinSinhVien(SinhVien SV)
        {
            Console.WriteLine(" Ma so: " + SV.MaSo_146);
            Console.WriteLine(" Ho ten: " + SV.HoTen_146);
            Console.WriteLine(" Lop sinh hoat: " + SV.Lopsh_146);
            Console.WriteLine(" Diem trung binh: " + SV.DiemTB_146);
        }
        static void TimKiemTheoTen(SinhVien[] SV, string Key)
        {
            foreach (SinhVien sv in SV)
            {
                if (sv.HoTen_146.Contains(Key))
                {
                    XuatThongTinSinhVien(sv);
                    Console.WriteLine();
                }
            }
        }
        public static void Main(string[] args)
        {
            SinhVien[] SV_146 = new SinhVien[4];
            SV_146[0].MaSo_146 = 1;
            SV_146[0].HoTen_146 = "Nguyen Van An";
            SV_146[0].Lopsh_146 = (Lopsh)1;
            SV_146[0].DiemTB_146 = 6.8;

            SV_146[1].MaSo_146 = 2;
            SV_146[1].HoTen_146 = "Hoang Thi Binh";
            SV_146[1].Lopsh_146 = (Lopsh)2;
            SV_146[1].DiemTB_146 = 5.3;

            SV_146[2].MaSo_146 = 3;
            SV_146[2].HoTen_146 = "Tran Anh Chi";
            SV_146[2].Lopsh_146 = (Lopsh)3;
            SV_146[2].DiemTB_146 = 8.8;

            SV_146[3].MaSo_146 = 4;
            SV_146[3].HoTen_146 = "Le Kim Dung";
            SV_146[3].Lopsh_146 = (Lopsh)2;
            SV_146[3].DiemTB_146 = 9.2;

            foreach (SinhVien sv in SV_146)
            {
                Console.WriteLine();
                XuatThongTinSinhVien(sv);
            }
            Console.WriteLine("Nhap ten sinh vien can tim kiem: ");
            string key = Console.ReadLine();
            TimKiemTheoTen(SV_146, key);

        }
    }
}
